import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import { 
  Link 
} from 'react-router-dom';



function SimpleDialog(props) {
  const { onClose, selectedValue, open } = props;

  const handleClose = () => {
    onClose(selectedValue);
  };

  return (
    <Dialog  onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
      <div class="flex flex-col items-center p-8">
        <div class="font-bold text-lg">About The App</div>
        <div>
        Input the starting location and then the location you want to call and uber to.
        The app will then tell you current Uber prices and will tell you the Uber price at set intervals of time.
        </div>
      </div>
    </Dialog>
  );
}

  SimpleDialog.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
  };


  export default function Home() {
    const [open, setOpen] = useState(false);

    const [currentLoc, setCurrentLoc] = useState("");
    const [destination, setDestination] = useState("");

    const handleClickOpen = () => {
      setOpen(true);
    };

    const handleClose = value => {
      setOpen(false);
    };

    const handleCurrentLocChange = (e) => {
      setCurrentLoc(e.target.value);
    }

    const handleDestinationChange = (e) => {
      setDestination(e.target.value);
    }

  
    
    return (
      <div class="flex flex-col items-center" >

        <div class='mt-8 font-semibold text-3xl'>
          Uber Price Notifier
        </div>

        <div class='mt-4 mb-8'>
          <Button variant="outlined" color="primary" onClick={handleClickOpen} >
            More Info
          </Button>
        </div>

        <SimpleDialog open={open} onClose={handleClose} />

        <div class="w-full md:w-1/2 px-3 md:mb-0">
          <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
            Current Location
          </label>
          <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-black-900 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-first-name" type="text" placeholder="123 Long St" value={currentLoc} onChange={handleCurrentLocChange} />            
        </div>

        <div class="w-full md:w-1/2 px-3 md:mb-0">
          <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
            Destination
          </label>
          <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-black-900 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-first-name" type="text" placeholder="456 Short Dr" value={destination} onChange={handleDestinationChange} />      
        </div>
        
          <Link 
            to={{
            pathname: "/InfoPage",
            data: [currentLoc, destination] // your data array of objects
          }}>
            <button class="mt-8 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
              Submit
            </button>
          </Link>

      </div>
    );
  }
