import React from 'react';
import { 
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import InfoPage from './InfoPage.js';
import Home from './Home.js';


export default function App() { 
  return(
    <Router basename="/react-tailwind-starter">
      <Switch>

        <Route exact path="/">
          <Home />
        </Route>

        <Route path="/InfoPage">
          <InfoPage />
        </Route>

      </Switch>
    </Router>
  );
}